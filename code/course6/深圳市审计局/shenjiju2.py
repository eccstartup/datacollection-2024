import os
import time
import requests
from bs4 import BeautifulSoup
from urllib.request import urlretrieve, build_opener, install_opener
import xlwt


headers = {
    'Cookie': r'Hm_lvt_8e27732e26e78ee7975a6f697a0d3bbf=1677850357,1678426339,1678680217,1679478512; zh_choose=n; arialoadData=false',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}

req = requests.get(r'http://audit.sz.gov.cn/postmeta/i/11991.json', headers=headers)
soup = BeautifulSoup(req.text, features='lxml')
articles = req.json()['articles']
for i, article in enumerate(articles):
    print(i, article['title'])
    print(article['url'])
    if article['type'] == 'attach' or article['linked_type'] == 'attach':
        pass
    else:
        pass
