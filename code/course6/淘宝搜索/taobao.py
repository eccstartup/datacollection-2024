from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time

service = Service(executable_path="/opt/homebrew/bin/chromedriver")
driver = webdriver.Chrome(service=service)

driver.get('https://www.taobao.com/')
search_bar = driver.find_element(By.CSS_SELECTOR, 'input#q')
search_bar.send_keys('南京审计大学\n')

time.sleep(30)
total_pages = driver.find_element(By.CSS_SELECTOR, 'div.total')
total_pages = int(total_pages.text.strip().lstrip('共').rstrip('页，'))

for page in range(total_pages):
    time.sleep(1)
    items = driver.find_elements(By.CSS_SELECTOR, 'div.item.J_MouserOnverReq')
    for item in items:
        a = item.find_element(By.CSS_SELECTOR, 'div.title a')
        print(a.text)

    next = driver.find_element(By.CSS_SELECTOR, 'a[trace="srp_bottom_pagedown"')
    next.click()
