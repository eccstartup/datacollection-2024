import os.path
import time

import requests
from datetime import datetime
from urllib.request import urlretrieve


user_agent = r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'

header = {
    'User-Agent': user_agent,
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
}


def data_i(i):
    return f'column=sse_main_latest&pageNum={i}&pageSize=30&sortName=&sortType=&clusterFlag=false'


req = requests.post(r'http://www.cninfo.com.cn/new/disclosure',
                    data=data_i(1),
                    headers=header)
totalpages = req.json()['totalpages']
print(totalpages)

if not os.path.exists('pdf'):
    os.makedirs('pdf')


def generate_url(bid, time1):
    return f'http://www.cninfo.com.cn/new/announcement/download?bulletinId={bid}&announceTime={time1}'


for i in range(1, totalpages + 1):
    req = requests.post(r'http://www.cninfo.com.cn/new/disclosure',
                        data=data_i(i),
                        headers=header)
    announcements = req.json()['announcements']
    for anno in announcements:
        title = anno['announcementTitle']
        time1 = anno['announcementTime']
        time1 = datetime.fromtimestamp(time1 // 1000)
        time1 = time1.strftime('%Y-%m-%d')
        annid = anno['announcementId']
        print(annid, title, time1)
        urlretrieve(generate_url(annid, time1), os.path.join('pdf', f'{title}.pdf'))
        time.sleep(2)
