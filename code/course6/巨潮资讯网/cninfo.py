import requests
import json
import time


user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'

header = {
    'User-Agent': user_agent,
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
}


def generate_data(page):
    return f'column=sse_main_latest&pageNum={page}&pageSize=30&sortName=&sortType=&clusterFlag=false'


req = requests.post(r'http://www.cninfo.com.cn/new/disclosure', data=generate_data(1), headers=header)
req_json = json.loads(req.text)
total_pages = req_json['totalpages']

print(req_json)

announcements = []
for i in range(1, total_pages+1):
    print(f'page {i}')
    req = requests.post(r'http://www.cninfo.com.cn/new/disclosure', data=generate_data(1), headers=header)
    req_json = json.loads(req.text)
    announcements.extend(req_json['announcements'])
    time.sleep(2)

with open('coinfo_anno.json', 'w') as f:
    json.dump(announcements, f)
