import scrapy


class XueshuSpider(scrapy.Spider):
    name = 'xueshu'
    allowed_domains = ['news.nau.edu.cn']
    start_urls = ['https://news.nau.edu.cn/5801/list.htm']

    def parse(self, response):
        for li in response.css('li.cols'):
            title = li.css('span a::text').get()
            date = li.css('span.cols_meta::text').get()
            yield {
                'title': title,
                'date': date
            }

        next_page = response.css('li.page_nav a.next::attr(href)').get()
        if next_page != 'javascript:void(0);':
            yield response.follow(next_page, callback=self.parse)
