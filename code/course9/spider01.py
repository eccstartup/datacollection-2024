import scrapy


class EmptySpider(scrapy.Spider):
    # name唯一，爬虫名字标识
    name = 'empty'
    # 允许的域名，非该域名下的链接不爬
    allowed_domains = []
    # 爬虫的起始url
    start_urls = []

    # 默认操作是把start_urls里面的链接编程Request(url, dont_filter=True)对象
    # 可以不写
    def start_requests(self):
        pass

    # 重要函数，用来爬取页面
    # 返回值是Request对象，或者scrapy的Item(s)
    def parse(self, response, **kwargs):
        pass