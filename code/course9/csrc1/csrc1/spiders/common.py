import scrapy


class CommonSpider(scrapy.Spider):
    name = 'common'
    allowed_domains = ['csrc.gov.cn']
    start_urls = ['http://www.csrc.gov.cn/csrc/c100040/common_list.shtml',
                  'http://www.csrc.gov.cn/csrc/c100040/common_list_2.shtml',
                  'http://www.csrc.gov.cn/csrc/c100040/common_list_3.shtml',
                  'http://www.csrc.gov.cn/csrc/c100040/common_list_4.shtml',
                  'http://www.csrc.gov.cn/csrc/c100040/common_list_5.shtml']

    def parse(self, response):
        ul = response.css('ul.list.mt10')[0]
        for li in ul.css('li'):
            title = li.css('a::text').get()
            date = li.css('span::text').get()
            yield {
                'title': title,
                'date': date
            }
