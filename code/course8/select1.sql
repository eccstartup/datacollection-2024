show tables;

select * from customers;
select * from employees;
select * from offices;
select * from orderdetails;
select * from orders;
select * from payments;
select * from productlines;
select * from products;

select distinct orderNumber from orderdetails;

select distinct orderNumber, productCode from orderdetails;

select count(distinct orderNumber) from orderdetails;

select count(*) from orders;

select * from employees where jobTitle like 'Sales%';

select * from offices where country <> 'USA';

select offices.addressLine1 from offices
join employees on employees.officeCode = offices.officeCode
where employees.lastName = 'Bow';

select offices.addressLine1, employees.* from offices
join employees on employees.officeCode = offices.officeCode
where employees.lastName = 'Bow';

# long example
select quantityOrdered * priceEach as price from orderdetails;

select sum(quantityOrdered * priceEach) as total_price from orderdetails;

select orderNumber, sum(quantityOrdered * priceEach) as total_price from orderdetails
group by orderNumber;

select orderNumber, customerNumber from orders order by orderNumber;

select customerNumber, customerName from customers;

select orders.orderNumber, customers.customerNumber, customers.customerName, sum(orderdetails.quantityOrdered * orderdetails.priceEach) as total_price
from orderdetails
join orders on orderdetails.orderNumber = orders.orderNumber
join customers on orders.customerNumber = customers.customerNumber
group by orderdetails.orderNumber;

# another example
select * from products limit 5;

select products.productCode, products.productName, sum(orderdetails.quantityOrdered * orderdetails.priceEach) as total_price from products
join orderdetails on products.productCode = orderdetails.productCode
group by products.productCode
order by total_price desc;

select products.productCode, products.productName, sum(orderdetails.quantityOrdered) as total_quantity from products
join orderdetails on products.productCode = orderdetails.productCode
group by products.productCode
order by total_quantity desc;

select productName from products
where productLine in ('Ships', 'Planes', 'Trains');

select * from products
where productName like '%Ferrari%';

select * from products
where productName like '%Porsche%';

