from sqlalchemy import Column, String, create_engine
from sqlalchemy.orm import declarative_base


Base = declarative_base()


class Paiwu(Base):
    __tablename__ = 'paiwu2'

    province = Column(String(255, collation='utf8mb4_general_ci'))
    city = Column(String(255, collation='utf8mb4_general_ci'))
    license = Column(String(255, collation='utf8mb4_general_ci'), primary_key=True)
    company = Column(String(255, collation='utf8mb4_general_ci'))
    companytype = Column(String(255, collation='utf8mb4_general_ci'))
    valid = Column(String(255, collation='utf8mb4_general_ci'))
    startdate = Column(String(255, collation='utf8mb4_general_ci'))


if __name__ == '__main__':
    url = 'mysql+mysqlconnector://ly:123456@localhost/test'
    engine = create_engine(url)
    meta = Base.metadata
    meta.create_all(engine)
