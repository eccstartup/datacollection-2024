import mysql.connector
import random


mydb = mysql.connector.connect(
    host="localhost",
    user="ly",
    password="123456",
    database='test'
)

mycursor = mydb.cursor()

mycursor.execute('SHOW TABLES;')

for x in mycursor:
    print(x)

s = 'abcdefghijklmnopqrstuvwxyz'


def generate_name():
    n = random.randint(4, 7)
    return ''.join([random.choice(s) for _ in range(n)]).capitalize()


def generate_age():
    return random.randint(1, 30)


# for i in range(10):
#     sql = f'insert into person(name, age) values (\'{generate_name()}\', {generate_age()})'
#     print(sql)
#     mycursor.execute(sql)
#
# mydb.commit()

# for i in range(20):
#     sql = f'insert into product(name, ownerId) values (\'{generate_name()}\', {random.choice(range(11, 21))})'
#     print(sql)
#     mycursor.execute(sql)
#
# mydb.commit()

