# 统计调查与数据采集

#### 介绍
统计调查与数据采集，2024年研究生课程资料

已结课，仓库存档

#### 教师
卢燚，南京审计大学讲师

#### 往年课件
* [2023年课件](https://gitee.com/eccstartup/datacollection-2023)
* [2022年课件](https://gitee.com/eccstartup/datacollection-2022)
* [2021年课件](https://gitee.com/eccstartup/datacollection-2021)

#### 配套课程视频
* [集智学园](https://campus.swarma.org/course/4815)